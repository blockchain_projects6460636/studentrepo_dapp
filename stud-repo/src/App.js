import React, { Component } from 'react';
import Web3 from 'web3';
import './App.css';
import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_studentrecord';
import StudentRecord from './components/StudentRecord'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
    }
    this.addStudent = this.addStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
  }

  addStudent(sid, name) {
    this.setState({ loading: true })
    this.state.studentRecord.methods.addStudent(sid, name)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  markGraduated(sid) {
    this.setState({ loading: true })
    this.state.studentList.methods.markGraduated(sid)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  componentDidMount() {
    if (!window.ethereum) {
      throw new Error("No crypto wallet found. Please install it.");
    }

    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData();
  }

  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545");
    const accounts = await web3.eth.getAccounts();
    // console.log(`Checking accounts: ${accounts}`);
    this.setState({ account: accounts[1] });

    const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS);
    this.setState({ studentRecord });

    const studentCount = await studentRecord.methods.studentsCount().call();
    this.setState({ studentCount });

    const students = [];
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentRecord.methods.students(i).call();
      students.push(student);
    }
    this.setState({ students });
  }


  render() {
    return (
      <div className="container">
        <h1>Student Record System</h1>
        <p>Your account: {this.state.account}</p>

        <StudentRecord
          addStudent={this.addStudent} />
          <ul id="studentList" className="list-unstyled">
              {this.state.students.map((student, key) => (

                  <li  className="list-group-item checkbox" key={key}>
                    <span className="name alert">{student._id}. {student.sid} {student.name}</span>
                    <input
                      className="form-check-input"
                      type="checkbox"
                      name={student._id}
                      defaultChecked={student.graduated}
                      disabled={student.graduated}
                      ref={(input) => {
                        this.checkbox = input;
                      }}
                      onClick={(event) => {
                        this.markGraduated(event.currentTarget.name)}}
                      />
                      <label className="form-check-label">Graduated</label>
                    </li>
                ))}
            </ul>
      </div>
    );
  }
}

export default App;
