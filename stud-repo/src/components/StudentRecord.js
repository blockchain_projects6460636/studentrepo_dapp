import React, { Component } from 'react';

class StudentRecord extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCourse: 'Choose', 
            selectedGender: 'Gender', 
        };
    }

    handleCourseChange = (event) => {
        this.setState({ selectedCourse: event.target.value });
    };

    handleGenderChange = (event) => {
        this.setState({ selectedGender: event.target.value });
    };

    render() {
        return (
            <form
                className="p-5 border"
                onSubmit={(event) => {
                    event.preventDefault();
                    this.props.addStudent(
                        this.sid.value,
                        this.student.value,
                        this.state.selectedCourse,
                        this.state.selectedGender
                    );
                }}
            >
                <h2>Add Student</h2>
                <input
                    id="newCID"
                    type="text"
                    ref={(input) => {
                        this.sid = input;
                    }}
                    className="form-control m-1"
                    placeholder="SID"
                    required
                />
                <input
                    id="newStudent"
                    type="text"
                    ref={(input) => {
                        this.student = input;
                    }}
                    className="form-control m-1"
                    placeholder="Student Name"
                    required
                />
                <select
                    className="form-control m-1"
                    onChange={this.handleCourseChange}
                    value={this.state.selectedCourse}
                >
                    <option value="Choose">Choose Course</option>
                    <option value="IT">IT</option>
                    <option value="Blockchain">Blockchain</option>
                    <option value="Fullstack">Fullstack</option>
                    <option value="AI">AI</option>
                    <option value="SIDD">SIDD</option>
                    <option value="Computer science">Computer Science</option>
                </select>
                <select
                    className="form-control m-1"
                    onChange={this.handleGenderChange}
                    value={this.state.selectedGender}
                >   
                    <option value="Gender">Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <input className="form-control btn-primary" type="submit" hidden="" />
            </form>
        );
    }
}

export default StudentRecord;
